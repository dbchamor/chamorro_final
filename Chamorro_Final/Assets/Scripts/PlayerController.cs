﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement; 

public class PlayerController : MonoBehaviour
{

    public GameEnding gameEnding; 

    public TextMeshProUGUI countText;
    public TextMeshProUGUI timeText;


    public float timeRemaining = 60;
    public bool timerIsRunning = false;

    public int count;
    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        SetCountText();
        timerIsRunning = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                timeRemaining = 0;
                timerIsRunning = false;
                SceneManager.LoadScene(4);
            }
        }

        
        if(Input.GetKeyDown(KeyCode.C))
        {
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f); //Shrinks the player
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);//grows the player after its been shrunk
        }
    }

    void OnTriggerEnter(Collider other) //Coding for the collectible
    {
        if (other.gameObject.CompareTag("Collectible"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
            print("One added");
            
        }
        if (other.gameObject.CompareTag("Collectible2"))
        {
            other.gameObject.SetActive(false);
            count = count + 2;
            SetCountText();
            print("Two added");
        }
        if (other.gameObject.CompareTag("Collectible3"))
        {
            other.gameObject.SetActive(false);
            count = count + 3;
            SetCountText();
            print("Three added");
        }

        /*if (other.gameObject.tag == "L1T")
        {
            print("Checkpoint 1 reset");
            gameEnding.CheckPoint1(); //Resets the player to Checkpoint 1
        }

        if(other.gameObject.tag == "L1T2")
        {
            print("Checkpoint 2 Reset");
            gameEnding.CheckPoint2();
        }

        if(other.gameObject.tag == "L1T3")
        {
            print("Checkpoint 3 reset");
            gameEnding.CheckPoint3();
        }

        if(other.gameObject.tag == "L2T1")
        {
            print("Checkpoint 4 reset");
            gameEnding.CheckPoint4();
        }

        if(other.gameObject.tag == "L2T2")
        {
            print("Checkpoint 5 reset");
            gameEnding.CheckPoint5();
        }*/
    }

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void SetCountText()
    {
        countText.text = "Rock: " + count.ToString();
    }

}
