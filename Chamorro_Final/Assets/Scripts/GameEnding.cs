﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class GameEnding : MonoBehaviour
{
    Vector3 checkpoint;
    public GameObject player;
    
    public PlayerController playerController; //Calls the player controller script

    /*public void CheckPoint1()
    {
        checkpoint = new Vector3(-45f, 11f, 26f);
        playerController.transform.position = checkpoint;
    }

    public void CheckPoint2()
    {
        checkpoint = new Vector3(14f, 0f, -17f);
        playerController.transfrom.position = checkpoint;

    }

    public void CheckPoint3()
    {
        checkpoint = new Vector3(4.0f, 3.0f, -17f);
        playerController.transform.position = checkpoint;
    }

    public void CheckPoint4()
    {
        checkpoint = new Vector3(33.0f, 0.0f, -28.0f);
        playerController.transform.position = checkpoint;
    }

    public void CheckPoint5()
    {
        checkpoint = new Vector3(69.0f, 0.0f, -26.0f);
        playerController.transform.position = checkpoint;

    }*/
    
    //SceneManager.LoadScene(4); //If the player fails to many times this fail scene will show

    // Update is called once per frame
    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            if(playerController.count >= 50 && playerController.timeRemaining > 0)
            {
                SceneManager.LoadScene(3); //If The player wins the game, this winning scene will display

            }
            
        }
    }
}
